package com.example.recyclerviewpractice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
public class MahasiswaAdapter extends
        RecyclerView.Adapter<MahasiswaAdapter.MahasiswaViewHolder> implements View.OnClickListener {
    private ArrayList<Mahasiswa> dataList;

    public MahasiswaAdapter(ArrayList<Mahasiswa>
                                    dataList) {
        this.dataList = dataList;
    }

    @Override
    public MahasiswaViewHolder
    onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        View view =
                layoutInflater.inflate(R.layout.row_mahasiswa, parent,
                        false);
        return new MahasiswaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MahasiswaViewHolder
                                         holder, int position) {

        holder.txtNama.setText(dataList.get(position).getNama());

        holder.txtNpm.setText(dataList.get(position).getNim());

        holder.txtNoHp.setText(dataList.get(position).getNohp());

        holder.itemView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    @Override
    public void onClick(View view) {
        TextView txtNama = (TextView) view.findViewById(R.id.txt_nama_mahasiswa);
        TextView txtNim = (TextView) view.findViewById(R.id.txt_nim_mahasiswa);
        TextView txtNoHp = (TextView) view.findViewById(R.id.txt_nohp_mahasiswa);
        Context context = view.getContext();
        String nama = txtNama.getText().toString();
        String nim = txtNim.getText().toString();
        String nohp = txtNoHp.getText().toString();
        Toast.makeText(context, "Halo ini " + nama, Toast.LENGTH_SHORT).show();
    }

    public class MahasiswaViewHolder extends
            RecyclerView.ViewHolder {
        private TextView txtNama, txtNpm, txtNoHp;

        public MahasiswaViewHolder(View itemView) {
            super(itemView);
            txtNama = (TextView)
                    itemView.findViewById(R.id.txt_nama_mahasiswa);
            txtNpm = (TextView)
                    itemView.findViewById(R.id.txt_nim_mahasiswa);
            txtNoHp = (TextView)
                    itemView.findViewById(R.id.txt_nohp_mahasiswa);
        }
    }
}
