```
Nama    : Arnoldy Fatwa Rahmadin
NIM     : 222011330
Kelas   : 3SI3
```

## Praktikum Pemrograman Platform Khusus Pertemuan 12

1. Dokumentasi Praktikum

    - Design tampilan activity_main.xml
        ![main](dokumentasi/layout_main.jpg)
    - Design tampilan row_mahasiswa.xml
        ![row](dokumentasi/row_mahasiswa_layout.jpg)

    - Output Ketika di Run
    ![output](dokumentasi/output1.jpg)

2. Menampilkan Toast

    - Tampilan ketika di klik pada row dengan nama "Dimas Maulana"
    ![dimas](dokumentasi/output_dimas.jpg)

    - Tampilan ketika di klik pada row dengan nama "Aham Siswana"
    ![aham](dokumentasi/output_aham.jpg)
